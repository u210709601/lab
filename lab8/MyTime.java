public class MyTime{
    public int hour;
    public int minute;

    public MyTime(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;

    }

    public String toString(){
        return this.hour  + ":" + this.minute;
    }

    public int incrementHour(int diff){
        int dayDiff = 0;
        if (hour + diff < 0){
            dayDiff = -1;
        }
        dayDiff += (hour + diff)/24;

        hour = (hour + diff)%24;

        if(hour< 0){
            hour += 24;
        }
        return dayDiff;

    }

    public  int incrementMinute(int diff){
        int hourDiff =0;

        hourDiff += (minute+diff)/60;
        minute = (minute+diff) %60;


        return  incrementHour(hourDiff);
    }



}