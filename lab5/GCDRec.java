import java.util.Scanner;
public class GCDRec {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("provide first number: ");
        int number1 = input.nextInt();

        System.out.print("provide second number: ");
        int number2 = input.nextInt();

        int result = GCDRec(number1>number2? number1:number2, number1>number2? number2:number1);
        System.out.println("GCD of these numbers: " + result);

    }
    public static int GCDRec(int num1, int num2){
        /*if(num2 == 0)
            return num1;
        else
            return GCDRec(num2, num1%num2);*/
        int remainder = num1 % num2;
        if(remainder == 0 )
            return  num2;
        return GCDRec(num2, remainder);
    }
}