import java.util.Scanner;
import java.util.Random;

public class GuessNumber2 {
    public static void main(String[] args) {
        Random rand = new Random();
        int rand_number = rand.nextInt(100);
        System.out.print("random number: " + rand_number + "\n");

        Scanner input = new Scanner(System.in);

        System.out.println("Hi! I'm thinking of a number between 0 and 99.");
        System.out.print("Can you guess it: ");
        int guess_number = input.nextInt();

        do {
            if (rand_number == guess_number) {
                System.out.println("Congratulations!");
                break;
            } else {
                System.out.print("Sorry!\n" + "Type -1 to quit or guess another: ");
                guess_number = input.nextInt();
            }
        }

        while(guess_number != -1);
        if(guess_number == -1)
            System.out.println("Sorry, the number was " + rand_number);

    }
}