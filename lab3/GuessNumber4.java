import java.util.Random;
import java.util.Scanner;

public class GuessNumber4 {
    public static void main(String[] args) {
        Random rand = new Random();
        int rand_number = rand.nextInt(100);
        System.out.print("random number: " + rand_number + "\n");

        Scanner input = new Scanner(System.in);

        System.out.println("Hi! I'm thinking of a number between 0 and 99.");
        System.out.print("Can you guess it: ");
        int guess_number = input.nextInt();

        int attemp = 1;
        while(guess_number!=rand_number && guess_number!=-1){
            System.out.println("Sorry!");
            if(guess_number<rand_number) {
                System.out.println("Mine is a greater than your guess.");
                System.out.print("Type -1 to quit or guess another: ");
            }
            if(guess_number > rand_number) {
                System.out.println("Mine is a smaller than your guess.");
                System.out.print("Type -1 to quit or guess another: ");
            }
            guess_number = input.nextInt();
            attemp +=1;
        }
        if(guess_number == rand_number)
            System.out.println("Congratulations!" + "\n" +  "You won after " + attemp + " attempts!");
        if(guess_number == -1)
            System.out.println("Sorry, the number was " + rand_number);
    }
}