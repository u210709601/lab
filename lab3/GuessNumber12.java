import java.util.Scanner;
import java.util.Random;

public class GuessNumber12 {
    public static void main(String[] args) {
        Random rand = new Random();
        int rand_number = rand.nextInt(100);
        System.out.print("random number: " + rand_number + "\n");

        Scanner input = new Scanner(System.in);

        System.out.println("Hi! I'm thinking of a number between 0 and 99.");
        System.out.print("Can you guess it: ");
        int guess_number = input.nextInt();

        if (guess_number == rand_number)
            System.out.println("Congratulations!");
        else {
            System.out.println("Hi! I'm thinking of a number between 0 and 99.");
            System.out.print("Can you guess it: ");
            guess_number = input.nextInt();
            if (guess_number == rand_number)
                System.out.println("Congratulations!");
            else
                System.out.println("Sorry, the number was " + rand_number);
            }
        }
    }