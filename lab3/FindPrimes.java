import java.util.Scanner;

public class FindPrimes {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("until which number:");
        int number = input.nextInt();

        for(int i=2; i<=number; i++){
            boolean bool = true;
            for(int j=2; j<i; j++){
                if(i%j == 0){
                    bool = false;
                    break;}
            }
            if (bool == true)
                System.out.print(i + ", " );
        }
    }
}