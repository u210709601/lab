package shapes2d;

public class Circle {
    //Define instance variables and area method for both of these classes

    protected double pi = 3.1415;
    protected int radius;

    public Circle(){
        this.radius = 5;
    }

    public Circle(int radius){
        this.radius = radius;
    }

    public double area(){
        return pi*radius*radius;
    }

    public String toString(){
        return "radius of the circle: " + this.radius + "\n" + "pi= " + this.pi;
    }
}
