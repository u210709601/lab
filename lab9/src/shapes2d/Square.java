package shapes2d;

public class Square {
    protected int side;

    public Square() {
        this.side = 5;
    }

    public Square(int side) {
        this.side = side;
    }

    public int area(){
        return this.side*this.side;
    }

    public String toString(){
        return "side of square: " + this.side;
    }
}
