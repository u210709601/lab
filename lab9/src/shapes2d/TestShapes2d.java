package shapes2d;

public class TestShapes2d {
    public static void main(String[] args) {
        Circle c1 = new Circle();
        c1.toString();
        System.out.println("area of circle: " + c1.area());

        Square s1 = new Square();
        s1.toString();
        System.out.println("area square: " + s1.area());
    }
}
