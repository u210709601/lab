package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {
    protected int height;

    public Cylinder(int radius, int height) {
        super(radius);
        this.height = height;
    }

    public double area(){
        return 2*super.area() + 2*super.pi*super.radius*height;
    }

    public double volume(){
        return super.area()*height;
    }

    public String toString(){
        return "radius of the cylinder's circle: " + this.radius + "\n" + "pi= " + this.pi +
                "\n" + "height of cylinder: " + height + "\n" + "area of cyliner's circle: " + super.area();
    }
}
