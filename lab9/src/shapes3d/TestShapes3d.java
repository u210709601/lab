package shapes3d;

public class TestShapes3d {
    public static void main(String[] args) {
        Cylinder c1 = new Cylinder(6, 5);
        System.out.println( c1.toString());
        System.out.println();
        System.out.println("volume of cylinder: " + c1.volume());
        System.out.println("area of cylinder: " + c1.area());

        System.out.println();

        Cube cube1 = new Cube(5);
        System.out.println(cube1.toString());
        System.out.println("area of cube: " + cube1.area());
        System.out.println("volume of cube: " + cube1.volume());
    }
}
