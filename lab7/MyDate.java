public class MyDate {
    int day, month, year;
    int[] maxDays = {31, 29, 31, 30,31,30,31,31,30,31,30,31};

    public boolean leapYear(){
        if(this.year%4==0)
            return  true;
        return false;
    }

    public MyDate(int d, int m, int y){
        this.day = d;
        this.month = m;
        this.year = y;
    }

    public void incrementDay(){
        this.day++;
        if(day>maxDays[this.month-1]){
            day = 1;
            this.month +=1;
            //month++;
        }
        else if(month==2 && day==29 && ! leapYear()){
            day = 1;
            month++;
        }
    }

    public void incrementYear(int year){
        this.year += year;
        if(this.year % 4 ==0 && this.month==2)
            this.day = 29;
        if(this.year % 4 !=0 && this.month==2)
            this.day = 28;
    }

    public void decrementDay() {
        if(day==1){
            day = maxDays[this.month-2];
            this.month -=1;
        }
        else if(month==3 && day==1 && ! leapYear()){
            day = 29;
            month--;
        }
        else
            this.day--;
    }

    public void decrementYear(){
        this.year -= 1;
        if(this.year % 4 ==0 && this.month==2)
            this.day = 29;
        if(this.year % 4 !=0 && this.month==2)
            this.day = 28;

    }

    public void decrementMonth(){
        this.month--;
    }

    public void incrementDay(int day) {
        while(day>0){
            incrementDay();
            day--;
        }
    }

    public void decrementMonth(int m){
        int diff_max = maxDays[this.month-1] - day;
        int difference = this.month - m;

        if(difference > 0){
            this.month = difference;
        }
        else if(difference < 0){
            while(difference < 0){
                difference += 12;
            }
            this.month = difference;
        }

        this.day = maxDays[this.month-1]-diff_max;

    }

    public void decrementDay(int day) {
        while(day>0){
            decrementDay();
            day--;
        }
    }

    public void incrementMonth(int m){
        this.month += m;
        int yearDiff = (month-1) /12;

        int newMonth= ((month-1) % 12)+1;
        this.month = newMonth<0 ? newMonth+12: newMonth;

        if(newMonth>0)
            yearDiff--;

        year += yearDiff;

        if(day>maxDays[month-1]){
            this.day = maxDays[month-1];
            if (this.month ==2 && day==29 && !leapYear()){
                this.day = 28;
            }
        }

    }


    public void decrementYear(int year){
        this.year -= year;
        if(this.year % 4 ==0 && this.month==2)
            this.day = 29;
        if(this.year % 4 !=0 && this.month==2)
            this.day = 28;
    }

    public void incrementMonth(){
        this.month++;

    }

    public void incrementYear(){
        this.year += 1;
        if(this.year % 4 ==0 && this.month==2)
            this.day = 29;
        if(this.year % 4 !=0 && this.month==2)
            this.day = 28;
    }

    public boolean isBefore(MyDate aDate){

        if(aDate.year > this.year)
            return true;
        else if(aDate.month > this.month)
            return true;
        else if (aDate.day > this.day)
            return true;
        return false;

    }

    public boolean isAfter(MyDate aDate){

        if(aDate.year < this.year)
            return true;
        else if(aDate.month < this.month)
            return true;
        else if (aDate.day < this.day)
            return true;
        return false;

    }

    public int dayDifference(MyDate aDate){
        int count = 0;
        while (!(aDate.day==this.day && aDate.month==this.month && aDate.year == this.year)){
            aDate.incrementDay();
            count++;
        }
        return count;
    }






    public String toString(){
        if(this.month<10 && this.day<10)
            return this.year + "-0" + this.month + "-0" + this.day;

        if(this.month<10)
            return this.year + "-0" + this.month + "-" + this.day;

        if(this.day<10)
            return this.year + "-" + this.month + "-0" + this.day;

        return this.year + "-" + this.month + "-" + this.day;
    }
}
