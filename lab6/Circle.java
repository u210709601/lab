public class Circle {
    public int radius;
    public Point center;
    public static double pi =3.1415;

    public Circle(int r, Point p){
        radius = r;
        center = p;
    }

    public double Area(){
        return pi*radius*radius;
    }

    public double Perimeter(){
        return 2*pi*radius;
    }

    public boolean Intersect(Circle c1){
        double distance = center.Distance(c1.center);
        if(distance <= radius+c1.radius){
            return true;
        }

        return false;
    }

}