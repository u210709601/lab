public class Main {
    public static void main(String[] args) {
        Point p1 = new Point(1,1);
        Rectangle r1 = new Rectangle(4, 5, p1);

        System.out.println("area of rectangle: " + r1.Area());
        System.out.println("perimeter of rectangle: " + r1.Perimeter());

        Point[] corners = r1.Corners(p1);

        System.out.println("Corners of rectangle: ");
        for(int i=0; i<4; i++){
            System.out.println(corners[i].xCoord + "," + corners[i].yCoord);
        }

        Point cc1 = new Point(1, 5);
        Circle c1 = new Circle(10, cc1);

        System.out.println("area of circle: " + c1.Area());
        System.out.println("perimeter of circle: " + c1.Perimeter());

        Point cc2 = new Point(1, 15);
        Circle c2 = new Circle(5, cc2);

        boolean result = c1.Intersect(c2);
        if(result)
            System.out.println("Circles intersect");
        else
            System.out.println("Circles are not intersect");

    }
}