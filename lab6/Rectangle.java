public class Rectangle {
    public int sideA;
    public int sideB;
    public Point topLeft;

    public Rectangle(int a, int b, Point p){
        sideA = a;
        sideB = b;
        topLeft = p;
    }

    public int Area(){
        return sideB*sideA;
    }

    public int Perimeter(){
        int perimeter = 2*sideA + 2*sideB;
        return perimeter;
    }

    public Point[] Corners(Point p) {
        Point[] corners = new Point[4];

        Point p1 = new Point(p.xCoord, p.yCoord);
        Point p2 = new Point(p.xCoord+sideA, p.yCoord);
        Point p3 = new Point(p.xCoord, p.yCoord+sideB);
        Point p4 = new Point(p.xCoord+sideA, p.yCoord+sideB);

        corners[0] = p1;
        corners[1] = p2;
        corners[2] = p3;
        corners[3] = p4;

        return corners;
    }


}