public class Point {
    public int xCoord;
    public int yCoord;

    public Point(int x, int y) {
        xCoord = x;
        yCoord = y;
    }

    public double Distance(Point p1) {
        double distance, pows;

        pows = Math.pow(p1.xCoord - xCoord, 2) + Math.pow(p1.yCoord - yCoord, 2);
        distance = Math.sqrt(pows);

        return distance;
    }
}