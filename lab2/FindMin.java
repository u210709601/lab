import java.util.Scanner;

public class FindMin {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("enter the first number: ");
        int first = input.nextInt();

        System.out.print("enter the second number: ");
        int second = input.nextInt();

        System.out.print("enter the third number: ");
        int third = input.nextInt();

        int smallest = first;

        if(second < smallest)
            smallest = second;

        if(third < smallest)
            smallest = third;

        System.out.println("smallest number: " + smallest);
    }
}