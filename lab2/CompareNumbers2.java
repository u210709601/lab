import java.util.Scanner;
public class CompareNumbers2 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        System.out.print("please enter first number: ");
        int first = input.nextInt();

        System.out.print("please enter second number: ");
        int second = input.nextInt();

        if(first > second)
            System.out.println(first + ">" + second);
        else if(second > first)
            System.out.println( second + ">" + first);
        else if(second == first)
                System.out.println(first + "=" + second);
        }
}