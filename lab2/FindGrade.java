import java.util.Scanner;
public class FindGrade {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("enter your score: ");
        int score = input.nextInt();

        boolean trust = (100>= score && score>=0) ? true:false;

        if(trust == false)
            System.out.println("invalid score");

        while(trust) {
            if (100 >= score && score >= 90){
                System.out.println("A");
                break;}
            else if (score >= 80){
                System.out.println("B");
                break;}
            else if (score >= 70){
                System.out.println("C");
                break;}
            else if (score >= 60){
                System.out.println("D");
                break;}
            else if (score >= 0){
                System.out.println("F");
                break;}
        }

    }
}